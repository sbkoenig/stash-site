<?php /* Template Name: Custom Stash Page */ get_header(); ?>
    <div id="background_image"></div>
    
    <!-- section -->
    <section role="main">
    
        <h1><?php the_title(); ?></h1>
    
    <?php if (have_posts()): while (have_posts()) : the_post(); ?>
    
        <!-- article -->
        
        <?php the_content(); ?>
                    
        <br class="clear">
        
        <?php edit_post_link(); ?>
            
        <!-- /article -->
        
    <?php endwhile; ?>
    
    <?php else: ?>
    
        <!-- article -->
        <article>
            
            <h2><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>
            
        </article>
        <!-- /article -->
    
    <?php endif; ?>
    
    </section>
    <!-- /section -->
    
<?php // get_sidebar(); ?>

<?php get_footer(); ?>