            <section id="footer_mailing_list">
                <article class="mailing-list">
                    <div>
                        <h1>Knit Inspired</h1>
                        <h2>Get Weekly Updates</h2>
                        <?php echo mailchimp_signup(); ?>
                    </div>
                </article>
            </section>
            <section id="stash_footer">
                <div id="stash_footer_nav">
                    <nav class="nav" role="navigation">
                        <?php html5blank_nav(); ?>
                    </nav>
                    <?php echo get_social_links(); ?>
                </div>
                <div id="stash_footer_address">
                    <address>
                        110 SW 3rd Street <br>
                        Corvallis, Oregon 97330 <br>
                        (541) 753-9276
                    </address>
                </div>
            </section>

            <!-- footer -->
            <!-- <footer class="footer" role="contentinfo"> -->
                
                <!-- copyright -->
                <!-- <p class="copyright"> -->
                    <!-- &copy; <?php echo date("Y"); ?> Copyright <?php bloginfo('name'); ?>. <?php _e('Powered by', 'html5blank'); ?>  -->
                    <!-- <a href="//wordpress.org" title="WordPress">WordPress</a> &amp; <a href="//html5blank.com" title="HTML5 Blank">HTML5 Blank</a>. -->
                <!-- </p> -->
                <!-- /copyright -->
                
            <!-- </footer> -->
            <!-- /footer -->
        
        </div>
        <!-- /wrapper -->

        <?php wp_footer(); ?>
        
        <!-- analytics -->
        <script>
            var _gaq=[['_setAccount','UA-XXXXXXXX-XX'],['_trackPageview']];
            (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
            g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';
            s.parentNode.insertBefore(g,s)})(document,'script');
        </script>
    
    </body>
</html>