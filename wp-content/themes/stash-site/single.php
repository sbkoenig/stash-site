<?php get_header(); ?>
    <div id="background_image"></div>

    <!-- section -->
    <section role="main" id="blog_content">
        <div id="blog_left">        
            <?php if (have_posts()): while (have_posts()) : the_post(); ?>
    
                <!-- article -->
                <article class="stash-post">
                    <div class="post-meta">
                        <h2><?php the_date(); ?>, <?php the_author(); ?></h2>
                    </div>
                    <div class="post-title">
                        <h1><?php the_title(); ?></h1>
                    </div>
                    <div class="post-content">
                        <?php the_content(); ?>
                    </div>
                                    
                        <br class="clear">
                </article>
                <!-- /article -->
                
            <?php endwhile; ?>

            <?php else: ?>
    
                <!-- article -->
                <article>
                    
                    <h2><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>
                    
                </article>
                <!-- /article -->
            
            <?php endif; ?>
        </div>
    

        <div id="blog_right">
            <article id="sidebar_social_media">
                <?php echo get_social_links(); ?>
            </article>
            
            <!-- Featured Posts -->
            <?php // Get featured posts (tag = "featured")
                query_posts('tag=featured&posts_per_page=3'); 
                while (have_posts()) : the_post(); 
            ?>
            <article class="featured-post">
                <div class="post-thumbnail">
                    <?php if ( has_post_thumbnail() ) the_post_thumbnail(); ?>
                </div>
                <div><h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2></div>
            </article>
            <?php endwhile; wp_reset_query(); ?>
            <!-- End Featured Posts -->

            <article id="wp_sidebar">
                <?php get_sidebar(); ?>
            </article>

        </div>
        
    </section>
    
<?php // get_sidebar(); ?>

<?php get_footer(); ?>