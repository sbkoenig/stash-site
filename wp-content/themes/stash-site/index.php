<?php get_header(); ?>
	<div id="background_image" class="blog-background-image"></div>

	<section id="blog_header">
		<article id="blog_header_title">
			<div>
				<h1>Take a break!</h1>
				<h2>Welcome to Your Inspired Knitting Community</h2>
			</div>
		</article>
		<hr>
		<article id="blog_mailing_list" class="mailing-list">
            <div>
                <h1>Dreaming in yarn?</h1>
                <h2>Unravel with us.</h2>
                <?php echo mailchimp_signup(); ?>
            </div>
        </article>
	</section>

	<!-- section -->
	<section role="main" id="blog_content">
		<div id="blog_left">		
			<?php get_template_part('loop'); ?>
			
			<?php get_template_part('pagination'); ?>
		</div>
	

		<div id="blog_right">
			<article id="sidebar_social_media">
                <?php echo get_social_links(); ?>
			</article>
	        
	        <!-- Featured Posts -->
	        <?php // Get featured posts (tag = "featured")
	        	query_posts('tag=featured&posts_per_page=3'); 
	        	while (have_posts()) : the_post(); 
        	?>
        	<article class="featured-post">
        		<div class="post-thumbnail">
		    		<?php if ( has_post_thumbnail() ) the_post_thumbnail(); ?>
				</div>
        		<div><h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2></div>
        	</article>
        	<?php endwhile; wp_reset_query(); ?>
        	<!-- End Featured Posts -->

        	<article id="wp_sidebar">
        		<?php get_sidebar(); ?>
        	</article>

		</div>
		
	</section>

<?php get_footer(); ?>