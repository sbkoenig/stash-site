<?php
/*
 *  Author: Todd Motto | @toddmotto
 *  URL: html5blank.com | @html5blank
 *  Custom functions, support, custom post types and more.
 */

/*
    Custom STASH Shortcodes
 */

function create_post_types()
{
    // Stash Class TYPE
    /*
    register_post_type( 'class',
        array(
            'labels' => array(
                'name' => __( 'Classes' ),
                'singular_name' => __( 'Class' ),
                'all_items' => 'All Classes',
                'add_new' => 'New Class',
                'edit_item' => __( 'Edit Class Details' ),
                'add_new_item' => 'Add New Class',
            ),
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'class'),
        )
    );
    */
    
    // yarnS type
    register_post_type( 'yarn',
        array(
            'labels' => array(
                'name' => __( 'yarns' ),
                'singular_name' => __( 'yarn' ),
                'all_items' => 'All yarns',
                'add_new' => 'New yarn',
                'edit_item' => __( 'Edit yarn Details' ),
                'add_new_item' => 'Add New yarn',
            ),
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'class'),
            'taxonomies' => array('yarn_type')
        )
    );

    $labels = array(
        'name'              => _x( 'yarn Types', 'taxonomy general name' ),
        'singular_name'     => _x( 'yarn Type', 'taxonomy singular name' ),
        'search_items'      => __( 'Search yarn Types' ),
        'all_items'         => __( 'All yarn Types' ),
        'parent_item'       => __( 'Parent yarn Type' ),
        'parent_item_colon' => __( 'Parent yarn Type:' ),
        'edit_item'         => __( 'Edit yarn Type' ),
        'update_item'       => __( 'Update yarn Type' ),
        'add_new_item'      => __( 'Add New yarn Type' ),
        'new_item_name'     => __( 'New yarn Type Name' ),
        'menu_name'         => __( 'Edit yarn Types' ),
    );

    $args = array(
        'hierarchical'      => true,
        'labels'            => $labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => array( 'slug' => 'yarn_type' ),
    );

    register_taxonomy( 'yarn_type', array( 'yarn' ), $args );

}

function show_classes($atts = NULL)
{
    // Defaults
    extract(shortcode_atts(array(
      // option defaults go here
    ), $atts));

    // query is made               
    query_posts('post_type=class');

    // the loop
    $output = '';
    if (have_posts()) : while (have_posts()) : the_post();

        $the_post = get_post($post->ID);
        $class_title = get_the_title($post->ID);
        $class_link = get_permalink($post->ID);

        // determine whether there is an image
        $meta = array(
            'class_image'          => get_field('class_image'           ,$post->ID),
            'class_instructor'     => get_field('class_instructor'      ,$post->ID),
            'class_instructor_url' => get_field('class_instructor_url'  ,$post->ID),
            'class_location'       => get_field('class_location'        ,$post->ID),
            'class_location_url'   => get_field('class_location_url'    ,$post->ID),
            'class_cost'           => get_field('class_cost'            ,$post->ID),
            'class_start_date_time'=> get_field('class_start_date_time' ,$post->ID),
            'class_end_date_time'  => get_field('class_end_date_time'   ,$post->ID),
            'skills_learned'       => get_field('skills_learned'        ,$post->ID),
            'skill_level'          => get_field('skill_level'           ,$post->ID),
            'class_materials'      => get_field('class_materials'       ,$post->ID),
        );

        $output .= html_template_stash_class($the_post, $meta);

    endwhile; endif;

    if(empty($output))
    {
        // print no classes
        $output = '<div class="empty">There are no classes as this time.</div>';
    }

    wp_reset_query();
    return $output;
}

function show_yarns($atts = NULL)
{
    extract(shortcode_atts(array(
        // defaults go here
        'type' => 'artisan'
    ), $atts));

    // query is made               
    $args = array(
        'post_type'    => 'yarn',
        'yarn_type' => $type
    );
    query_posts( $args );
    // query_posts('post_type=yarn');

    // the loop
    $output = '';
    if (have_posts()) : while (have_posts()) : the_post();

        $the_post    = get_post($post->ID);
        $class_title = get_the_title($post->ID);
        $class_link  = get_permalink($post->ID);

        // determine whether there is an image
        $meta = array(
            'yarn_image' => get_field('yarn_image'       ,$post->ID),
            'yarn_cost'  => get_field('yarn_cost'        ,$post->ID),
            'pinterest_link'  => get_field('pinterest_link'    ,$post->ID),
        );

        $output .= html_template_stash_yarn($the_post, $meta);

    endwhile; endif;

    if(empty($output))
    {
        // print no yarns
        $output = '<div class="empty">There are no yarns in this category</div>';
    }

    wp_reset_query();
    return "<div class=\"stash-yarns-section\">{$output}</div>";
}

/**
 * gets all instagram photos from IG
 *
 * requires token and user id
 * 
 * 1. To retrieve your access token and user ID, go here:
 * https://api.instagram.com/oauth/authorize/?client_id=ab103e54c54747ada9e807137db52d77&redirect_uri=http://blueprintinteractive.com/tutorials/instagram/uri.php&response_type=code
 * (it's ok)
 *
 * 2. use code from: http://www.blueprintinteractive.com/blog/how-instagram-api-fancybox-simplified
 *    
 * @return string - the html
 */
function get_instagram_feed()
{
    $instagram_username = 'stashlocal';

    // grab all images through an ajax call. since
    // IG takes forever to respond with posts
    $feed_url = get_template_directory_uri() . '/stash-lib/instagram-feed.php';

    // this section must have a unique id
    $ig_id = 'igfeed' . rand();

    $icon_html = "<div class=\"instagram-icon\"><a title=\"click to see {$instagram_username}'s Instagram\" href=\"https://instagram.com/{$instagram_username}/\"><span>IG</span></a></div>";

    // we started with an empty string, and now we're here.
    $html = "";

    // script will be rendered underneath the html
    $ig_script = "
        <script type=\"text/javascript\">
            var wrapper = $('#{$ig_id}');
            var slider = $('#{$ig_id} .instagram-feed-slider');

            // keep things within this id, in 
            // case there is another IG on same page.
            slider.load('{$feed_url}', function() {
                
                slider.carouFredSel({
                    circular: false,
                    infinite: false,
                    auto    : false,
                    width : '100%',
                    height: 250,
                    prev    : { 
                        button  : \"#{$ig_id} .instagram-ctrl-left\",
                        key     : \"left\"
                    },
                    next    : { 
                        button  : \"#{$ig_id} .instagram-ctrl-right\",
                        key     : \"right\"
                    },
                    pagination  : {
                        container : \"#{$ig_id} .instagram-feed-pagination\",
                        anchorBuilder : function(nr, item) {
                            return '<a href=\"#'+nr+'\"><span class=\"icon-circle\"></span></a>';
                        }
                    }
                });

                $(\"#{$ig_id} a.ig_feed\").fancybox({
                    'nextEffect'    : 'fade',
                    'prevEffect'    : 'fade',
                    'overlayOpacity': 0.8,
                    'overlayColor'  : '#000000',
                    'arrows' : true,
                    'titlePosition' : 'inside',
                });

                // once finished with everything, show loaded
                wrapper.addClass('loaded');
                // fade in the slider and it's siblings
                wrapper.find('> div').add(slider).animate({opacity: 1});
                
            });

        </script>
    ";

    $ig_controls = "
        <div class=\"instagram-ctrl-left\"><span class=\"icon-chevron-sign-left\"></span></div>
        <div class=\"instagram-ctrl-right\"><span class=\"icon-chevron-sign-right\"></span></div>
    ";

    $pagination = "<div class=\"instagram-feed-pagination\"></div>";

    return "<div class=\"instagram-feed\" id=\"{$ig_id}\">{$pagination}{$ig_controls}<div class=\"instagram-feed-slider\">{$html}</div>{$icon_html}</div>{$ig_script}";
}
// end ig feed


/**
 * find image links custom fields for the page the
 * viewer is currently on
 *
 * @author Daniel Walker <daniel.walker@assistrx.com>
 * @since  2013-09-01
 * @return string the html with the image links
 */
function get_image_links()
{
    // find the id of the current page
    $current_page_id = get_the_ID();

    $links = get_field('bottom_links', $current_page_id);

    // the loop
    $output = '';
    foreach ($links as $link)
    {
        // just in case there is no image.
        $image = $link['image'] ? "style=\"background-image: url('{$link['image']}');\" " : '';

        // just in case there is no link:
        $href = $link['page_link'] ? "onclick=\"window.location='{$link['page_link']}';\"" : '';
        $class = $link['page_link'] ? 'clickable' : '';

        $output .= "
            <div {$image} {$href} class=\"stash-image-link {$class}\">
                <div class=\"stash-image-link-title\">
                    {$link['title']}
                </div>
                <div class=\"stash-image-link-content\">
                    {$link['content_text']}
                </div>
                <div class=\"stash-image-link-footer\">
                    {$link['footer_text']}
                </div>
            </div>
        ";
    }

    if(!empty($output))
    {
        // print no yarns
        $output = "<div class=\"stash-image-links\">{$output}</div>";
    }
    else
    {
        $output = "<!-- NO BOTTOM IMAGE LINKS TO DISPLAY -->";
    }

    wp_reset_query();
    return $output;
}

function get_map_view()
{
    // url to a bing maps:
    $url = 'http://www.bing.com/maps/?v=2&amp;cp=44.564014~-123.255933&amp;lvl=16&amp;dir=0&amp;sty=r&amp;q=110%20SW%203rd%20Street%20%20Corvallis%2C%20Oregon%2097330&amp;form=LMLTEW';

    $html = "
        <div class=\"the_map_view\">
            <a class=\"map_view\" href=\"{$url}\" title=\"click to see larger map\" target=\"_blank\">
            </a>
            <a class=\"map_link\" href=\"{$url}\" title=\"click to see larger map\" target=\"_blank\">
                Click to See Larger Map
            </a>
        </div>
    ";

    return $html;
}

function get_social_links()
{
    $links = array(
        'ravelry' => 'http://www.ravelry.com/groups/stash-2',
        'facebook' => 'https://www.facebook.com/StashLocal',
        'pinterest' => 'http://pinterest.com/stashlocal',
        'twitter' => 'https://twitter.com/StashLocal',
        'youtube' => 'http://www.youtube.com/user/StashLocal',
    );

    $item_html = '';

    foreach ($links as $name => $url)
    {
        $item_html .= "<a href=\"{$url}\" class=\"{$name}\" title=\"{$name}\"></a>";
    }

    return "
        <div class=\"social-media\">
            {$item_html}
        </div>
    ";
}

/**
 * shortcode for listing yarn types 
 *
 * i.e. <a href="#artisan" title="click to scroll to artisan yarns">Artisan</a> &bull;
 * 
 * @return string - html fof types
 */
function get_yarn_table_of_contents()
{
    $headers = array();

    // query the taxonomies for categories
    $cats = get_terms('yarn_type', array( 'taxonomy' => 'yarn_type'));
    foreach ($cats as $key => $tax)
    {
        $headers[] = "<a href=\"#{$tax->name}\" title=\"click to scroll to {$tax->name} yarns\">{$tax->name}</a>";
    }

    return implode(' &bull; ', $headers);
}

/*------------------------------------*\
    External Modules/Files
\*------------------------------------*/

// Load any external files you have here

/*------------------------------------*\
    Theme Support
\*------------------------------------*/

if (!isset($content_width))
{
    $content_width = 900;
}

if (function_exists('add_theme_support'))
{
    // Add Menu Support
    add_theme_support('menus');

    // Add Thumbnail Theme Support
    add_theme_support('post-thumbnails');
    add_image_size('large', 700, '', true); // Large Thumbnail
    add_image_size('medium', 250, '', true); // Medium Thumbnail
    add_image_size('small', 120, '', true); // Small Thumbnail
    add_image_size('custom-size', 700, 200, true); // Custom Thumbnail Size call using the_post_thumbnail('custom-size');

    // Add Support for Custom Backgrounds - Uncomment below if you're going to use
    /*add_theme_support('custom-background', array(
    'default-color' => 'FFF',
    'default-image' => get_template_directory_uri() . '/img/bg.jpg'
    ));*/

    // Add Support for Custom Header - Uncomment below if you're going to use
    /*add_theme_support('custom-header', array(
    'default-image'         => get_template_directory_uri() . '/img/headers/default.jpg',
    'header-text'           => false,
    'default-text-color'        => '000',
    'width'             => 1000,
    'height'            => 198,
    'random-default'        => false,
    'wp-head-callback'      => $wphead_cb,
    'admin-head-callback'       => $adminhead_cb,
    'admin-preview-callback'    => $adminpreview_cb
    ));*/

    // Enables post and comment RSS feed links to head
    add_theme_support('automatic-feed-links');

    // Localisation Support
    load_theme_textdomain('html5blank', get_template_directory() . '/languages');
}

/*------------------------------------*\
    Functions
\*------------------------------------*/

/**
 * function to render the mailchimp signup form
 */
function mailchimp_signup()
{
    $id = 'signup' . rand();
    $msg_id = 'message' . rand();

    // must include link to theme directory
    $signup_url = get_template_directory_uri() . '/stash-lib/signup.php';

    return <<<EOD
    <form id="{$id}" action="index.html" method="post">
        <input type="hidden" name="ajax" value="true" />
        <input type="text" placeholder="Name" name="stash_name" id="stash_name">
        <input type="text" placeholder="Email" name="stash_email" id="stash_email">
        <button type="submit">Yes!</button>
    </form>
    <div id="{$msg_id}"></div>

    <script type="text/javascript">
    $(document).ready(function() {
        var signup = $('#{$id}');
        var message = $('#{$msg_id}');

        signup.submit(function() {
            message.html("<span class='going'>Adding your email address...</span>");

            // before clearing the fields, grab data
            var form_data = signup.serialize();

            // clear form inputs of type text (not the hidden one)
            signup.find('input[type=text]').val('');

            $.ajax({
                type : 'post',
                dataType: "json",
                // proper url to your "store-address.php" file
                url: "{$signup_url}",
                data: form_data,
                success: function(res) {
                    var msg_class = res.success ? 'success' : 'failure';

                    // in bot cases, display the message
                    message.html("<span class='" + msg_class + "'>" + res.message + "</span>");
                }
            });
            // return false to not submit the html form.
            return false;
        });
    });
    </script>

EOD;
}

// html builder for classes dw
// meta must contain all keys:
// + class_image
// + class_instructor
// + class_instructor_url
// + class_location
// + class_location_url
// + class_cost
// + class_start_date_time
// + class_end_date_time
// + skills_learned
// + skill_level
// + class_materials
function html_template_stash_class($class, $meta = array())
{
    // var_dump($class);
    // var_dump($meta);

    // if an image was supplied, wrap it in the div for the left
    $image_html = '<!-- NO IMAGE -->';

    if(!empty($meta['class_image']))
    {
        $image_html = "<div class=\"stash-class-image\"><img alt=\"Image for {$class->post_title}\" src=\"{$meta['class_image']}\" /></div>";
    }

    // if instructor has url, include it in instructor text
    if(!empty($meta['class_instructor_url']))
    {
        $meta['class_instructor'] = "<a href=\"{$meta['class_instructor_url']}\" title=\"Click to See {$meta['class_instructor']}'s Website\">{$meta['class_instructor']}</a>";
    }

    // if location has url, include it in location text
    if(!empty($meta['class_location_url']))
    {
        $meta['class_location'] = "<a href=\"{$meta['class_location_url']}\" title=\"Click to See {$meta['class_location']}'s Website\">{$meta['class_location']}</a>";
    }

    /* because skills_learned is a repeater, compute it here
     *  ["skills_learned"]=>
     *  array(2) {
     *    [0]=>
     *    array(1) {
     *      ["skill_learned"]=>
     *      string(11) "programming"
     *    }
     *    [1]=>
     *    array(1) {
     *      ["skill_learned"]=>
     *      string(8) "knitting"
     *    }
     *  }
     */
    $skills_learned = '<!-- no skills to learn... -->';

    if(!empty($meta['skills_learned']))
    {
        $skills_learned = "<p class=\"stash-class-skills\"><strong>Skills Learned:</strong> ";
        $skills = array();

        // for each skill, add to string.
        // 
        foreach ($meta['skills_learned'] as $skill)
        {
            $skills[] = $skill['skill_learned'];
        }

        // concantenate and add commas, close out the paragraph also,
        $skills_learned .= implode(', ', $skills) . "</p>";
    }

    // build out the object using meta data
    return "
        <div class=\"stash-class\">

            <!-- START IMAGE -->
            <!-- if no image, this will be empty string -->
            {$image_html}
            <!-- END IMAGE -->

            <!-- HEADING -->
            <div class=\"stash-class-header\">
                
                <!-- START TITLE -->
                <h2 class=\"stash-class-title\">
                    {$class->post_title}
                </h2>
                <!-- END TITLE -->
                
                <p>
                    <span class=\"stash-class-instructor\">
                        with {$meta['class_instructor']}
                    </span>

                    <!-- START DATE -->
                    <strong>
                        <span class=\"stash-class-start-date\">
                            {$meta['class_start_date_time']}
                        </span>

                        <span class=\"stash-class-end-date\">
                            {$meta['class_end_date_time']}
                        </span>
                    </strong>
                    <!-- END DATE -->
                </p>
                
            </div>
            <!-- END HEADER -->

            <!-- START CONTENT -->
            <div class=\"stash-class-content\">
                <p>{$class->post_content}</p>
            </div>
            <!-- END CONTENT -->

            <!-- START FOOTER -->
            <div class=\"stash-class-footer\">
                
                <p class=\"stash-class-materials\">
                    <strong>Materials:</strong> 
                    {$meta['class_materials']}
                </p>

                {$skills_learned}

                <p class=\"stash-class-skill-level\">
                    <strong>Skill Level:</strong> 
                    {$meta['skill_level']}
                </p>

                <p class=\"stash-class-location\">
                    <strong>Location:</strong>
                    {$meta['class_location']}
                </p>

                <p class=\"stash-class-cost\">
                    <strong>Cost:</strong>
                    {$meta['class_cost']}
                </p>
            </div>
            <!-- END FOOTER -->

        </div>
    ";
}


// html builder for stash yarns dw
// meta must contain all keys:
// + yarn_image
// + yarn_cost
// + pinterest_link
// DESCRIPTION is the post content
// type is a taxonomy so, this function will only be called on the 
// correct yarn_types found by query_posts()
function html_template_stash_yarn($yarn, $meta = array())
{
    // var_dump($yarn);
    // var_dump($meta);

    // if an image was supplied, wrap it in the div for the left
    $image_html = '<!-- NO IMAGE -->';

    if(!empty($meta['yarn_image']))
    {
        $pinterest = '';

        if($meta['pinterest_link'])
        {
            $pinterest = "<a class=\"stash-yarn-pinterest\" href=\"{$meta['pinterest_link']}\" data-pin-do=\"buttonPin\" data-pin-config=\"above\" target=\"_blank\"><img src=\"//assets.pinterest.com/images/pidgets/pin_it_button.png\" /></a>";
        }

        $image_html = "<div class=\"stash-yarn-image\">{$pinterest}<img alt=\"Image for {$yarn->post_title}\" src=\"{$meta['yarn_image']}\" /></div>";
    }

    if(!empty($meta['yarn_cost']))
    {
        $meta['yarn_cost'] = "
            <p class=\"stash-yarn-cost\">
                <strong>Cost:</strong> 
                {$meta['yarn_cost']}
            </p>";
    }

    // build out the object using meta data
    return "
        <div class=\"stash-yarn\">

            <!-- HEADING -->
            <div class=\"stash-yarn-header\">
                
                <!-- START IMAGE -->
                <!-- if no image, this will be empty string -->
                {$image_html}
                <!-- END IMAGE -->

                <!-- START TITLE -->
                <h2 class=\"stash-yarn-title\">
                    {$yarn->post_title}
                </h2>
                <!-- END TITLE -->

            </div>
            <!-- END HEADER -->

            <!-- START CONTENT -->
            <div class=\"stash-yarn-content\">
                <p>{$yarn->post_content}</p>
            </div>
            <!-- END CONTENT -->

            <!-- START FOOTER -->
            <div class=\"stash-yarn-footer\">

                {$meta['yarn_cost']}

            </div>
            <!-- END FOOTER -->

        </div>
    ";
}

// HTML5 Blank navigation
function html5blank_nav()
{
    wp_nav_menu(
    array(
        'theme_location'  => 'header-menu',
        'menu'            => '', 
        'container'       => false, 
        'container_class' => '', 
        'container_id'    => '',
        'menu_class'      => 'menu', 
        'menu_id'         => '',
        'echo'            => true,
        'fallback_cb'     => 'wp_page_menu',
        'before'          => '',
        'after'           => '',
        'link_before'     => '',
        'link_after'      => '',
        // the wrapper needs an extra div to match the html expected by the css
        'items_wrap'      => '<div class="menu"><ul>%3$s</ul></div>',
        'depth'           => 0,
        'walker'          => ''
        )
    );
}

// Load HTML5 Blank scripts (header.php)
function html5blank_header_scripts()
{
    if (!is_admin()) {
    
        wp_deregister_script('jquery'); // Deregister WordPress jQuery
        wp_register_script('jquery', 'http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js', array(), '1.9.1'); // Google CDN jQuery
        wp_enqueue_script('jquery');
        
        wp_deregister_script('fancybox'); // just in case
        wp_register_script('fancybox', 'http://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.pack.js', array(), '2.1.5'); // CDN JS Facybox
        wp_enqueue_script('fancybox');

        wp_register_script('conditionizr', 'http://cdnjs.cloudflare.com/ajax/libs/conditionizr.js/2.2.0/conditionizr.min.js', array(), '2.2.0'); // Conditionizr
        wp_enqueue_script('conditionizr');
        
        wp_register_script('modernizr', 'http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.6.2/modernizr.min.js', array(), '2.6.2'); // Modernizr
        wp_enqueue_script('modernizr');
        
        wp_register_script('html5blankscripts', get_template_directory_uri() . '/js/scripts.js', array(), '1.0.0'); // Custom scripts
        wp_enqueue_script('html5blankscripts');

        wp_register_script('caroufredsel', get_template_directory_uri() . '/js/jquery.carouFredSel-6.2.1-packed.js', array(), '6.2.1');
        wp_enqueue_script('caroufredsel');

        // wp_register_script('pinterest-pinit', 'http://assets.pinterest.com/js/pinit.js', array(), 'unknown-version');
        // wp_enqueue_script('pinterest-pinit');
    }
}

// Load HTML5 Blank conditional scripts
function html5blank_conditional_scripts()
{
    if (is_page('pagenamehere')) {
        wp_register_script('scriptname', get_template_directory_uri() . '/js/scriptname.js', array('jquery'), '1.0.0'); // Conditional script(s)
        wp_enqueue_script('scriptname');
    }
}

// Load HTML5 Blank styles
function html5blank_styles()
{
    wp_register_style('normalize', get_template_directory_uri() . '/normalize.css', array(), '1.0', 'all');
    wp_enqueue_style('normalize');

    wp_register_style('fancybox', get_template_directory_uri() . '/css/fancybox/jquery.fancybox.css', array(), '2.1.5', 'all');
    wp_enqueue_style('fancybox');

    wp_register_style('fontawesome', 'http://netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.min.css', array(), '3.2.1', 'all');
    wp_enqueue_style('fontawesome');

    wp_register_style('stashstyle', get_template_directory_uri() . '/style.css', array(), '1.0', 'all');
    wp_enqueue_style('stashstyle');
}

// Register HTML5 Blank Navigation
function register_html5_menu()
{
    register_nav_menus(array( // Using array to specify more menus if needed
        'header-menu' => __('Header Menu', 'html5blank'), // Main Navigation
        'sidebar-menu' => __('Sidebar Menu', 'html5blank'), // Sidebar Navigation
        'extra-menu' => __('Extra Menu', 'html5blank') // Extra Navigation if needed (duplicate as many as you need!)
    ));
}

// Remove the <div> surrounding the dynamic navigation to cleanup markup
function my_wp_nav_menu_args($args = '')
{
    $args['container'] = false;
    return $args;
}

// Remove Injected classes, ID's and Page ID's from Navigation <li> items
function my_css_attributes_filter($var)
{
    return is_array($var) ? array() : '';
}

// Remove invalid rel attribute values in the categorylist
function remove_category_rel_from_category_list($thelist)
{
    return str_replace('rel="category tag"', 'rel="tag"', $thelist);
}

// Add page slug to body class, love this - Credit: Starkers Wordpress Theme
function add_slug_to_body_class($classes)
{
    global $post;
    if (is_home()) {
        $key = array_search('blog', $classes);
        if ($key > -1) {
            unset($classes[$key]);
        }
    } elseif (is_page()) {
        $classes[] = sanitize_html_class($post->post_name);
    } elseif (is_singular()) {
        $classes[] = sanitize_html_class($post->post_name);
    }

    return $classes;
}

// If Dynamic Sidebar Exists
if (function_exists('register_sidebar'))
{
    // Define Sidebar Widget Area 1
    register_sidebar(array(
        'name' => __('Widget Area 1', 'html5blank'),
        'description' => __('Description for this widget-area...', 'html5blank'),
        'id' => 'widget-area-1',
        'before_widget' => '<div id="%1$s" class="%2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3>',
        'after_title' => '</h3>'
    ));

    // Define Sidebar Widget Area 2
    register_sidebar(array(
        'name' => __('Widget Area 2', 'html5blank'),
        'description' => __('Description for this widget-area...', 'html5blank'),
        'id' => 'widget-area-2',
        'before_widget' => '<div id="%1$s" class="%2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3>',
        'after_title' => '</h3>'
    ));
}

// Remove wp_head() injected Recent Comment styles
function my_remove_recent_comments_style()
{
    global $wp_widget_factory;
    remove_action('wp_head', array(
        $wp_widget_factory->widgets['WP_Widget_Recent_Comments'],
        'recent_comments_style'
    ));
}

// Pagination for paged posts, Page 1, Page 2, Page 3, with Next and Previous Links, No plugin
function html5wp_pagination()
{
    global $wp_query;
    $big = 999999999;
    echo paginate_links(array(
        'base' => str_replace($big, '%#%', get_pagenum_link($big)),
        'format' => '?paged=%#%',
        'current' => max(1, get_query_var('paged')),
        'total' => $wp_query->max_num_pages
    ));
}

// Custom Excerpts
function html5wp_index($length) // Create 20 Word Callback for Index page Excerpts, call using html5wp_excerpt('html5wp_index');
{
    return 20;
}

// Create 40 Word Callback for Custom Post Excerpts, call using html5wp_excerpt('html5wp_custom_post');
function html5wp_custom_post($length)
{
    return 40;
}

// Create the Custom Excerpts callback
function html5wp_excerpt($length_callback = '', $more_callback = '')
{
    global $post;
    if (function_exists($length_callback)) {
        add_filter('excerpt_length', $length_callback);
    }
    if (function_exists($more_callback)) {
        add_filter('excerpt_more', $more_callback);
    }
    $output = get_the_excerpt();
    $output = apply_filters('wptexturize', $output);
    $output = apply_filters('convert_chars', $output);
    $output = '<p>' . $output . '</p>';
    echo $output;
}

// Custom View Article link to Post
function html5_blank_view_article($more)
{
    global $post;
    return '... <a class="view-article" href="' . get_permalink($post->ID) . '">' . __('Keep Reading', 'html5blank') . '</a>';
}

// Remove Admin bar
function remove_admin_bar()
{
    return false;
}

// Remove 'text/css' from our enqueued stylesheet
function html5_style_remove($tag)
{
    return preg_replace('~\s+type=["\'][^"\']++["\']~', '', $tag);
}

// Remove thumbnail width and height dimensions that prevent fluid images in the_thumbnail
function remove_thumbnail_dimensions( $html )
{
    $html = preg_replace('/(width|height)=\"\d*\"\s/', "", $html);
    return $html;
}

// Custom Gravatar in Settings > Discussion
function html5blankgravatar ($avatar_defaults)
{
    $myavatar = get_template_directory_uri() . '/img/gravatar.jpg';
    $avatar_defaults[$myavatar] = "Custom Gravatar";
    return $avatar_defaults;
}

// Threaded Comments
function enable_threaded_comments()
{
    if (!is_admin()) {
        if (is_singular() AND comments_open() AND (get_option('thread_comments') == 1)) {
            wp_enqueue_script('comment-reply');
        }
    }
}

// Custom Comments Callback
function html5blankcomments($comment, $args, $depth)
{
    $GLOBALS['comment'] = $comment;
    extract($args, EXTR_SKIP);
    
    if ( 'div' == $args['style'] ) {
        $tag = 'div';
        $add_below = 'comment';
    } else {
        $tag = 'li';
        $add_below = 'div-comment';
    }
?>
    <!-- heads up: starting < for the html tag (li or div) in the next line: -->
    <<?php echo $tag ?> <?php comment_class(empty( $args['has_children'] ) ? '' : 'parent') ?> id="comment-<?php comment_ID() ?>">
    <?php if ( 'div' != $args['style'] ) : ?>
    <div id="div-comment-<?php comment_ID() ?>" class="comment-body">
    <?php endif; ?>
    <div class="comment-author vcard">
    <?php if ($args['avatar_size'] != 0) echo get_avatar( $comment, $args['180'] ); ?>
    <?php printf(__('<cite class="fn">%s</cite> <span class="says">says:</span>'), get_comment_author_link()) ?>
    </div>
<?php if ($comment->comment_approved == '0') : ?>
    <em class="comment-awaiting-moderation"><?php _e('Your comment is awaiting moderation.') ?></em>
    <br />
<?php endif; ?>

    <div class="comment-meta commentmetadata"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>">
        <?php
            printf( __('%1$s at %2$s'), get_comment_date(),  get_comment_time()) ?></a><?php edit_comment_link(__('(Edit)'),'  ','' );
        ?>
    </div>

    <?php comment_text() ?>

    <div class="reply">
    <?php comment_reply_link(array_merge( $args, array('add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
    </div>
    <?php if ( 'div' != $args['style'] ) : ?>
    </div>
    <?php endif; ?>
<?php }

/*------------------------------------*\
    Actions + Filters + ShortCodes
\*------------------------------------*/


// Add Actions
add_action( 'init', 'create_post_types' ); // STAHS types - class, yarns
add_action('init', 'html5blank_header_scripts'); // Add Custom Scripts to wp_head
add_action('wp_print_scripts', 'html5blank_conditional_scripts'); // Add Conditional Page Scripts
add_action('get_header', 'enable_threaded_comments'); // Enable Threaded Comments
add_action('wp_enqueue_scripts', 'html5blank_styles'); // Add Theme Stylesheet
add_action('init', 'register_html5_menu'); // Add HTML5 Blank Menu
add_action('widgets_init', 'my_remove_recent_comments_style'); // Remove inline Recent Comment Styles from wp_head()
add_action('init', 'html5wp_pagination'); // Add our HTML5 Pagination

// Remove Actions
remove_action('wp_head', 'feed_links_extra', 3); // Display the links to the extra feeds such as category feeds
remove_action('wp_head', 'feed_links', 2); // Display the links to the general feeds: Post and Comment Feed
remove_action('wp_head', 'rsd_link'); // Display the link to the Really Simple Discovery service endpoint, EditURI link
remove_action('wp_head', 'wlwmanifest_link'); // Display the link to the Windows Live Writer manifest file.
remove_action('wp_head', 'index_rel_link'); // Index link
remove_action('wp_head', 'parent_post_rel_link', 10, 0); // Prev link
remove_action('wp_head', 'start_post_rel_link', 10, 0); // Start link
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0); // Display relational links for the posts adjacent to the current post.
remove_action('wp_head', 'wp_generator'); // Display the XHTML generator that is generated on the wp_head hook, WP version
remove_action('wp_head', 'start_post_rel_link', 10, 0);
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
remove_action('wp_head', 'rel_canonical');
remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);

// Add Filters
add_filter('avatar_defaults', 'html5blankgravatar'); // Custom Gravatar in Settings > Discussion
add_filter('body_class', 'add_slug_to_body_class'); // Add slug to body class (Starkers build)
add_filter('widget_text', 'do_shortcode'); // Allow shortcodes in Dynamic Sidebar
add_filter('widget_text', 'shortcode_unautop'); // Remove <p> tags in Dynamic Sidebars (better!)
add_filter('wp_nav_menu_args', 'my_wp_nav_menu_args'); // Remove surrounding <div> from WP Navigation
// add_filter('nav_menu_css_class', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> injected classes (Commented out by default)
// add_filter('nav_menu_item_id', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> injected ID (Commented out by default)
// add_filter('page_css_class', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> Page ID's (Commented out by default)
add_filter('the_category', 'remove_category_rel_from_category_list'); // Remove invalid rel attribute
add_filter('the_excerpt', 'shortcode_unautop'); // Remove auto <p> tags in Excerpt (Manual Excerpts only)
add_filter('the_excerpt', 'do_shortcode'); // Allows Shortcodes to be executed in Excerpt (Manual Excerpts only)
add_filter('excerpt_more', 'html5_blank_view_article'); // Add 'View Article' button instead of [...] for Excerpts
add_filter('show_admin_bar', 'remove_admin_bar'); // Remove Admin bar
add_filter('style_loader_tag', 'html5_style_remove'); // Remove 'text/css' from enqueued stylesheet
add_filter('post_thumbnail_html', 'remove_thumbnail_dimensions', 10); // Remove width and height dynamic attributes to thumbnails
add_filter('image_send_to_editor', 'remove_thumbnail_dimensions', 10); // Remove width and height dynamic attributes to post images

// Remove Filters
remove_filter('the_excerpt', 'wpautop'); // Remove <p> tags from Excerpt altogether

// Shortcodes
add_shortcode('html5_shortcode_demo', 'html5_shortcode_demo'); // You can place [html5_shortcode_demo] in Pages, Posts now.
add_shortcode('html5_shortcode_demo_2', 'html5_shortcode_demo_2'); // Place [html5_shortcode_demo_2] in Pages, Posts now.

// stash shortcodes---
// add short code for classes, no attributes used
add_shortcode("classes", "show_classes");
add_shortcode("yarns", "show_yarns"); // attr: type
add_shortcode("mailchimp_signup", "mailchimp_signup");
add_shortcode("instagram", "get_instagram_feed");
add_shortcode("image_links", "get_image_links");
add_shortcode("map_view", "get_map_view");
add_shortcode("yarn_table_of_contents", "get_yarn_table_of_contents");
add_shortcode("social_links", "get_social_links");


// Shortcodes above would be nested like this -
// [html5_shortcode_demo] [html5_shortcode_demo_2] Here's the page title! [/html5_shortcode_demo_2] [/html5_shortcode_demo]

/*------------------------------------*\
    Custom Post Types
\*------------------------------------*/

// Create 1 Custom Post type for a Demo, called HTML5-Blank
function create_post_type_html5()
{
    register_taxonomy_for_object_type('category', 'html5-blank'); // Register Taxonomies for Category
    register_taxonomy_for_object_type('post_tag', 'html5-blank');
    register_post_type('html5-blank', // Register Custom Post Type
        array(
        'labels' => array(
            'name' => __('HTML5 Blank Custom Post', 'html5blank'), // Rename these to suit
            'singular_name' => __('HTML5 Blank Custom Post', 'html5blank'),
            'add_new' => __('Add New', 'html5blank'),
            'add_new_item' => __('Add New HTML5 Blank Custom Post', 'html5blank'),
            'edit' => __('Edit', 'html5blank'),
            'edit_item' => __('Edit HTML5 Blank Custom Post', 'html5blank'),
            'new_item' => __('New HTML5 Blank Custom Post', 'html5blank'),
            'view' => __('View HTML5 Blank Custom Post', 'html5blank'),
            'view_item' => __('View HTML5 Blank Custom Post', 'html5blank'),
            'search_items' => __('Search HTML5 Blank Custom Post', 'html5blank'),
            'not_found' => __('No HTML5 Blank Custom Posts found', 'html5blank'),
            'not_found_in_trash' => __('No HTML5 Blank Custom Posts found in Trash', 'html5blank')
        ),
        'public' => true,
        'hierarchical' => true, // Allows your posts to behave like Hierarchy Pages
        'has_archive' => true,
        'supports' => array(
            'title',
            'editor',
            'excerpt',
            'thumbnail'
        ), // Go to Dashboard Custom HTML5 Blank post for supports
        'can_export' => true, // Allows export in Tools > Export
        'taxonomies' => array(
            'post_tag',
            'category'
        ) // Add Category and Post Tags support
    ));
}

/*------------------------------------*\
    ShortCode Functions
\*------------------------------------*/

// Shortcode Demo with Nested Capability
function html5_shortcode_demo($atts, $content = null)
{
    return '<div class="shortcode-demo">' . do_shortcode($content) . '</div>'; // do_shortcode allows for nested Shortcodes
}

// Shortcode Demo with simple <h2> tag
function html5_shortcode_demo_2($atts, $content = null) // Demo Heading H2 shortcode, allows for nesting within above element. Fully expandable.
{
    return '<h2>' . $content . '</h2>';
}

?>
