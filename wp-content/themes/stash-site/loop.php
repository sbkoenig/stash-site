<?php get_header(); ?>
    
    <?php if (have_posts()): while (have_posts()) : the_post(); ?>
    <article>

    	<div class="post-meta">
    		<h2><?php the_date(); ?>, <?php the_author(); ?></h2>
    	</div>

		<div class="post-thumbnail">
    		<?php if ( has_post_thumbnail() ) the_post_thumbnail(); ?>
		</div>

        <div class="post-title">
            <h1><?php the_title(); ?></h1>
        </div>
        
        
        <!-- article -->
        <div class="post-excerpt">
            <?php the_excerpt(); ?>
                        
            <br class="clear">
            
            <?php edit_post_link(); ?>
        </div>
            
        <!-- /article -->
    </article>
        
    <?php endwhile; ?>
    
    <?php else: ?>
    
        <!-- article -->
        <article>
            
            <h2><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>
            
        </article>
        <!-- /article -->
    
    <?php endif; ?>
    
<?php // get_sidebar(); ?>